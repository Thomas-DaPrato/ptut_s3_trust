#ifndef RSAPUBLICKEY_H
#define RSAPUBLICKEY_H

#include <gmpxx.h>
#include "rsakey.h"

namespace nsCrypto
{
    class RSAPrivateKey;
    class RSAPublicKey : public RSAKey
    {
    protected:
        RSAPublicKey(const mpz_class e, const mpz_class n);

        friend class RSAPrivateKey;
    }; // class RSAPublicKey
} // namespace nsCrypto

#endif // RSAPUBLICKEY_H
