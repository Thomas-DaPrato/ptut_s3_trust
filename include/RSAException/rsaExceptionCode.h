#ifndef SIGNATUREEXCEPTIONCODE_H
#define SIGNATUREEXCEPTIONCODE_H

namespace nsCrypto
{
    enum RSAExceptionCode
    {
        NoKey,
        ModulusTooShort,
        SignatureLengthException,
        InvalidKeyLength,
        PrimalityError
    };
}

#endif // SIGNATUREEXCEPTIONCODE_H
