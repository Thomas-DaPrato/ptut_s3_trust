\documentclass[class=article, crop=false]{standalone}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{fancyvrb}
\usepackage{fvextra}

\begin{document}

\section{Attaque avec courbe invalide}

\subsection{Introduction}

En annexe, nous décrivons le fonctionnement de l'algorithme Diffie Hellman avec les courbes elliptiques (ECDH : Elliptic curve Diffie Hellman). Dans cette partie, nous allons nous intéresser à une attaque qui repose sur l'utilisation de groupes de petite taille. L'objectif est de "forcer" un utilisateur à travailler avec un point qui n'est pas sur la bonne courbe. Dans un premier temps, nous allons présenter l'attaque, puis nous présenterons un exemple de recherche de courbe invalide avec SageMath. Pour éviter ce type d'attaque, les implémentations doivent vérifier que les points traités sont bien sur la courbe originale. Si cette précaution n'est pas prise, ECDH peut être vulnérable.

\subsection{Fonctionnement}

Avec une équation de la forme
\[y^2 = x^3 + ax + b\]
On observe que les opérations d'addition ne nécessitent pas l'utilisation de \(b\). Un utilisateur qui ne vérifie pas si le point utilisé est sur la bonne courbe peut alors effectuer des opérations d'addition tout à fait valide (bien que la courbe ne soit pas sûre). Pour cela, il faut s'assurer que l'équation de la courbe sur laquelle travaille réellement l'utilisateur est de la forme
\[y^2 = x^3 + ax + c\]
Avec \(c \neq b\). \\
On choisit alors une courbe dont l'ordre est composé (avec de petits facteurs pour faciliter l'attaque). \\

Plaçons nous dans le cas d'un échange entre un client et un serveur. Nous notons : \\
\(G\) : le point de base, son ordre doit être élevé (taille du sous-groupe engendré par G) \\
\(d_c\) : le secret du client \\
\(d_s\) : le secret du serveur \\

Le client peut calculer \(d_cG\) et l'envoyer au serveur. Le serveur peut calculer \(d_sG\) et l'envoyer au client. Le client et le serveur sont alors capables de calculer le secret partagé : \(d_s(d_cG) = d_c(d_sG)\). \\

Si le client fait en sorte que le serveur travaille sur une courbe différente de celle d'origine (\(c \neq d)\), il sera par la suite beaucoup plus facile de retrouver la clé privée du serveur. \\

Supposons que le client a une telle courbe ainsi qu'un point \(P\) d'ordre \(m\) très petit (de plus, on suppose que le serveur ne vérifie pas si les points traités sont sur la courbe). Dans un premier temps, au lieu de calculer \(d_cG\), il envoie directement \(P\) au serveur qui va donc calculer \(d_sP\). S'il parvient à se procurer \(d_sP\), l'attaquant (le client) peut alors déterminer \((d_s \mod m)\) car \(d_sP = (d_s \mod m)P\) (avec \(m\) l'ordre de \(P\)). \\
En répétant cela plusieurs fois avec des points d'ordres \(m_1, m_2, ...\) différents (et deux à deux premiers entre eux), il obtient :

\[d_s \mod m_1\]
\[d_s \mod m_2\]
\[...\]

Il es possible d'utiliser le théorème des restes chinois pour retrouver le secret \(d_s\) (nous ne traitons pas ce théorème dans ce dossier). Voici un document sur le déroulement complet de cette attaque :
\href{https://www.nds.ruhr-uni-bochum.de/media/nds/veroeffentlichungen/2015/09/14/main-full.pdf}{Practical Invalid Curve Attacks on TLS-ECDH}. Ce document décrit aussi l'utilisation de cette attaque contre TLS, ce qui permet de retrouver la clé privée (le secret) du serveur. Cela fonctionne avec ECDH (statique) qui utilise toujours le même secret; deux utilisateurs donnés obtiendrons toujours le même secret partagé avec ECDH (statique). Si l'on parvient à retrouver \(d_s\), puisque \(d_cG\) est connu, on peut donc recalculer le secret partagé \(d_s(d_cG)\), cela permet de déchiffrer les conversations passées et à venir. \\
Avec TLS 1.3, ECDHE (ECDH Ephemeral) est utilisé, dans ce cas les secrets des parties sont générés pour chaque exécution de ECDHE, retrouver \(d_s\) ne permet que de déchiffrer au plus une session (mais retrouver \(d_s\) est bien plus difficile puisque la valeur n'est utilisée qu'un seule fois).

\subsection{Recherche d'un point sur une courbe invalide}

Nous allons maintenant montrer un exemple de recherche d'un point d'ordre donnée sur une courbe invalide. Pour cela, nous nous basons sur la courbe P-256 dont les paramètres sont les suivants :
\[p = 2^{256} - 2^{224} + 2^{192} + 2^{96} - 1\]
\[a = -3\]
\[b = 41058363725152142129326129780047268409114441015993725554835256314039467401291\]

L'équation de la courbe est donc :
\[y^2 = x^3-3x+41058363725152142129326129780047268409114441015993725554835256314039467401291\]

\begin{minted}{Python}
# courbe P-256
p = 2**(256) - 2**(224) + 2**(192) + 2**(96) - 1
b = 0x5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b
a = -3
\end{minted}

Dans un premier temps nous allons chercher l'ordre du groupe.

\begin{minted}{Python}
E = EllipticCurve(GF(p), [a, b])
n = E.cardinality()
print('factor', factor(n))
print('card : ', n)
\end{minted}

\begin{Verbatim}[breaklines=true, breakanywhere=true]
('factor', 115792089210356248762697446949407573529996955224135760342422259061068512044369)
('card : ', 115792089210356248762697446949407573529996955224135760342422259061068512044369)
\end{Verbatim}

L'ordre est premier. \\

Cherchons maintenant une courbe dont l'ordre est composé en ne modifiant que la valeur de \(b\). On se rend compte que quelques essais suffisent pour trouver une telle courbe.

\begin{minted}{Python}
for i in range(0, 6) :
	print('i = ', i)
	# il faut un discriminant non nul mod p
	if GF(p)((4 * (a**3)) + (27*(i**2))) != 0 :
		E2 = EllipticCurve(GF(p), [a, i])
		print('factor(E2.cardinality()) : ', factor(E2.cardinality()))
		print('card : ', E2.cardinality())
	else :
		print('discriminant nul')
	print()
\end{minted}

\begin{Verbatim}[breaklines=true, breakanywhere=true]
('i = ', 0)
('factor(E2.cardinality()) : ', 2^96 * 7 * 274177 * 67280421310721 * 11318308927973941931404914103)
('card : ', 115792089210356248762697446949407573530086143415290314195533631308867097853952)


('i = ', 1)
('factor(E2.cardinality()) : ', 71 * 823 * 1229 * 7489 * 30203 * 1275057701 * 5590685618197487513633360286414913886653362665945221)
('card : ', 115792089210356248762697446949407573529578712231346505346655645343696123459399)


('i = ', 2)
discriminant nul


('i = ', 3)
('factor(E2.cardinality()) : ', 2^4 * 3 * 7 * 13 * 37 * 97 * 113 * 65364863466230065009299609518518318921279098632127819301704248465833)
('card : ', 115792089210356248762697446949407573529995394580452997270780266901612618829008)


('i = ', 4)
('factor(E2.cardinality()) : ', 2^2 * 13 * 19 * 179 * 13003 * 1307093479 * 218034068577407083 * 16884307952548170257 * 10464321644447000442097)
('card : ', 115792089210356248762697446949407573530301458765764575276748425375978192226668)


('i = ', 5)
('factor(E2.cardinality()) : ', 2^2 * 3 * 7 * 13 * 2447 * 43333351749528183857746664058033075385207938864390055103100636196733549)
('card : ', 115792089210356248762697446949407573530623378430069411602317684396560437888076)
\end{Verbatim}

Choisissons par exemple la dernière courbe (\(i = 5\)), l'ordre du groupe est composé. Nous devons maintenant trouver un générateur du groupe. Encore un fois, nous faisons ceci par essais successifs (génération aléatoires de points).

\begin{minted}{Python}
found = False
E2 = EllipticCurve(GF(p), [a, 5])
while not found :
	P = E2.random_point()
	print('factor(P.order()) : ', factor(P.order())) # toujours même chose que l'ordre du groupe ??
	if E2.cardinality() == P.order() :
		print(P)
		found = True
		print('est generateur')
	print()
\end{minted}
Nous avons ainsi trouvé le point suivant :
\begin{Verbatim}[breaklines=true, breakanywhere=true]
('factor(P.order()) : ', 2^2 * 3 * 7 * 13 * 2447 * 43333351749528183857746664058033075385207938864390055103100636196733549)
(89906044049919325362173913473769761853217202552611422918918216647398370832285 : 20230419956724720466169712476502840738644664836504518094934394645899049166377 : 1)
est generateur
\end{Verbatim}
Notons \(G = (89906044049919325362173913473769761853217202552611422918918216647398370832285,\) \\ \(20230419956724720466169712476502840738644664836504518094934394645899049166377)\) \\
et \(m\) l'ordre de \(G\). \\

Parmi les diviseurs de l'ordre de G on trouve notamment \(2, 3, 7, 13\). Ces nombres divisent \(m\) donc pour \(h \in \{2, 3, 7, 13\}\) on a \(\frac{m}{h}\) entier et on peut calculer \(G_h = (\frac{m}{h})G\) qui est un point d'ordre \(h\). 

\[hG_h = h((\frac{m}{h})G) = mG = O\]

Cherchons par exemple un point d'ordre \(2\).

\begin{minted}{Python}
G = E2(89906044049919325362173913473769761853217202552611422918918216647398370832285,
20230419956724720466169712476502840738644664836504518094934394645899049166377)
G2 = int(G.order() / 2) * G
print('G2.order()', G2.order())
print('G2 : ', G2)
\end{minted}

\begin{Verbatim}[breaklines=true, breakanywhere=true]
('G2.order()', 2)
('G2 : ', (98819847942428805742002354006386840019676525869315184973139125710807339875491 : 0 : 1))
\end{Verbatim}
\(G_2 = (98819847942428805742002354006386840019676525869315184973139125710807339875491, 0)\) \\

Voici le résultat de la recherche d'un point d'ordre \(3\) :

\begin{Verbatim}[breaklines=true, breakanywhere=true]
('G3.order()', 3)
('G3 : ', (78469638840160311570777346908003411797239715976488900947729813450069695639753 : 91521923260775102746290593815414717292635064470271501608590137734869451604808 : 1))
\end{Verbatim}
\(G_3 = (78469638840160311570777346908003411797239715976488900947729813450069695639753,\)
\(91521923260775102746290593815414717292635064470271501608590137734869451604808)\) \\

Nous pouvons ainsi trouver des points dont l'ordre est un diviseur de l'ordre de \(G\). Les points que nous trouvons par cette méthode peuvent être utilisés pour une attaque de type "invalid-curve".

\paragraph{Référence \\}
\url{https://tools.ietf.org/html/rfc8017}
\url{https://owasp.org/www-pdf-archive/Practical_Invalid_Curve_Attacks_on_TLS-ECDH_-_Juraj_Somorovsky.pdf}
\url{https://web-in-security.blogspot.com/2015/09/practical-invalid-curve-attacks.html}
\url{https://www.nds.ruhr-uni-bochum.de/media/nds/veroeffentlichungen/2015/09/14/main-full.pdf}

\end{document}
