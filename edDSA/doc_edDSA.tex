\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}

\title{Documentation EdDSA (Edwards-curve Digital Signature Algorithm)}
\date{Décembre 2020}

\begin{document}

\maketitle
\tableofcontents
\newpage


\section{Introduction}
EdDSA est un algorithme de signature numérique moderne et sécurisé basé sur les courbes elliptiques optimisées pour les performances, telles que la courbe de 255 bits Curve25519 et la courbe de 448 bits Curve448-Goldilocks. Les signature EdDSA utilisent la forme Edwards des courbes elliptiques (pour des raisons de performances), respectivement edwards25519 et edwards 448. L’algorithme EdDSA est basé sur l’algorithme de signature de Schnorr et repose sur la difficulté du problème du logarithme discret permettant ainsi de pallier aux soucis de sécurité que l’on pouvait trouver dans d’autres fonctions de signature numérique telle que RSA ou encore DSA. \newline
Ed25529 et Ed448 sont des instances  spécifiques de la famille EdDSA et sont spécifiés dans la RFC 8032.\newline

\begin{figure}[h!]
    \includegraphics{courbe_edward_tordue.png}
    \caption{Courbe d’Edwards tordue d’équation : \(10x^2 + y^2 = 1+ 6x^2y^2\)}
\end{figure}

\newpage

\section{Paramètre de la courbe}

Une courbe d’Edward tordue admet comme équation : \(E_{a,d} ax^2 + y^2 = 1 + dx^2y^2\)

EdDSA admet 11 paramètres pour “fonctionner” : \newline
\begin{enumerate}
    \item un entier \(p\) premier impair (permet de savoir sur quel groupe fini il faut se placer : ici \(GF(p)\) ou plus généralement \(\mathbb{Z}/p\mathbb{Z}\))
    \item un entier \(b\) avec \(2^{b-1} > p\). Il permet de définir la taille de la clé publique (\(b\) bits) ainsi que la signature (\(2*b\) bits). Il est recommandé d’avoir un \(b\) multiple de \(8\) pour avoir des octets finis (la taille des clés publiques et des signatures sont sur des nombres entiers d'octet)
    \item un encodage de \(b-1\) bits pour les éléments sur \(GF(p)\)
    \item une fonction de hachage qui ressort \(b-1\) bits. Il est préférable d’utiliser une fonction de hachage qui ne crée pas de collision
    \item un entier \(c\) qui prend la valeur 2 ou 3. Le scalaire secret de edDSA (obtenue en hachant la clé privée avec sha512. Ce hash va générer 2 secrets : le premier est celui du scalaire secret et le second est utilisé pour le schéma de signature) est multiple de $2^c$. De plus, $c$ est le logarithme en base 2 du cofacteur (voir génération des clés pour plus de précision sur le cofacteur).
    \item un entier $n$ avec $c <= n < b$. Le scalaire secret de edDSA a exactement $n+1$ bits
    \item un entier $d$ qui n'est pas carré parfait (ex: 25 est un carré parfait mais 5 ne l’est pas car la racine carré de 5 n’est pas un nombre entier), le plus proche de zéros et qui donne une courbe acceptable
    \item un entier $a$ carré et non-nul. Il est recommandé de prendre $a = 1$ quand $p \mod 4 =1$ et $a = -1$ quand $p \mod 4 = 3$
    \item un élément $B$ qui n’est pas $0$ et $1$ et qui appartient à l’ensemble $E$ définit comme $(x,y)$ un membre de $GF(p)$ ou $x$ appartient à $GF(p)$ et vérifie l’équation $a * x^2 y^2 = 1 + d * x^2 * y^2$
    \item un entier $L$ tel que $[L]B = 0$ et $2^c * L = E$ ou $E$ est le nombre de points sur la courbe  
    \item une fonction de préhash \newline
\end{enumerate}


On a donc les paramètres suivant pour la courbe ed25519 : \newline
\begin{itemize}

    \item $p = 2^{255} - 19$
    \item $b = 256$
    \item $255$ bits d’encodage
    \item sha-512 pour la fonction de hachage
    \item $c = 3$
    \item $n = 254$ 
    \item $d = 37095705934669439343138083508754565189542113879843219016388785533085940283555$
    \item $a = -1$
    \item $B = X(p),Y(p) =$ \newline
    $15112221349535400772501151409588531511454012693041857206046113283949847762202$,\newline
    $46316835694926478169428394003475163141307993866256225615783033603165251855960$
    \item $L = 2^{252}+27742317777372353535851937790883648493$
    \item $PH(x) = x$
\end{itemize}    

\section{Addition de point sur EdDSA}
    
En ce qui concerne l’addition de 2 points sur ed25519, et plus généralement sur les courbes utilisant EdDSA comme schéma de signature, on procède ainsi :
\begin{center}
    $x3 = (x1 * y2 + x2 * y1) / (1 + d * x1 *x2 *y1 *y2)$
    $y3 = (y1 * y2 -a *x1 * x2) / (1 + d * x1 *x2 *y1 *y2 )$
\end{center}
$a$ et $d$ représente un élément non-nul de l’ensemble $\mathbb{K}$ (ensemble \mathbb{R} ou \mathbb{C}) et on été définis plus haut .\newline\newline
On peut aussi représenté les coordonnées $(x,y)$ sous la forme d’extended twisted Edwards coordinates (ou coordonnée étendue de la courbe d’Edward tordue) : 
$(x,y)$ $\rightarrow$ $(X,Y,Z,T)$ avec $x = X / Z$, $y = Y / Z$ et $x * y = T / Z$. \newline
On fixe Z = 1 pour ainsi avoir des points sur la courbe.
Ces coordonnées permettent d’utiliser l’algorithme suivant : \newline
    \[A = (Y_1-X_1)*(Y_2-X_2)\]
    \[B = (Y_1+X_1)*(Y_2+X_2)\]
    \[C = T_1*2*d*T_2\]
    \[D = Z_1*2*Z_2\]
    \[E = B-A\]
    \[F = D-C\]
    \[G = D+C\]
    \[H = B+A\]
    \[X_3 = E*F\]
    \[Y_3 = G*H\]
    \[T_3 = E*H\]
    \[Z_3 = F*G\]

On a donc :
\begin{center}
    \[x_3 = X_3 / Z_3\]
    \[\Leftrightarrow x_3 = E * F / F * G\]
    \[\Leftrightarrow x_3 = E / G\]
    \[\Leftrightarrow x_3 = (B - A) / (D + C)\]
    \[\Leftrightarrow x_3 = ((Y1+X1)*(Y2+X2) - (Y1-X1)*(Y2-X2)) / Z1*2*Z2 + T1*2*d*T2\]
    \[\Leftrightarrow x_3 = (X1 * Y2 + X2 * Y1) / ( Z2 * Z1 + T1*d*T2)\]
\end{center}  

On peut exprimer X = x / Z, Y = y / Z, T = (x * y) / Z
Or Z = 1 et on a donc  X = x, Y = y, T = x * y
On remplace donc et on retrouve bien x3 =  (x1 * y2 + x2 * y1) / (1 + d * x1 *x2 *y1 *y2)

On fait de même avec y3 : 
\begin{center}
    \[\Leftrightarrow y_3 = Y3 / Z3\]
    \[\Leftrightarrow y_3 = G * H / G * F\]
    \[\Leftrightarrow y_3 = H / F\]
    \[\Leftrightarrow y_3 = ((Y1+X1)*(Y2+X2) + (Y1-X1)*(Y2-X2)) / Z1*2*Z2 - T1*2*d*T2\]
    \[\Leftrightarrow y_3 = (Y1*Y2 + X1*X2) / (Z1 *Z2 - T1*T2*d)\]
\end{center}
   

On remplace par les même variables définies plus haut et on trouve y3 =  (y1 * y2 + x1 * x2) / (1- d * x1 *x2 *y1 *y2 ).\newline
Ce n’est pas tout à fait la même chose car dans la première équation on a un -a qui multiplie x1*x2. Comme dans la plupart des cas on prend a = -1 (a = 1 pour Ed448), l’équation est donc vérifiée. Les 2 méthodes sont donc équivalente. 

Cet algorithme n'est utilisable que pour ed25519. Pour Ed448, un autre algorithme est utilisé.



\section{Génération de clés EdDSA}

Ed25519 et Ed448 utilisent en même temps de petites clés privées et clés publiques (32 ou 57 octets) et de petites signatures (64 ou 114 octets) avec un niveau de sécurité élevé (respectivement 128 bits ou 224 bits). La sécurité des bits mesure le nombre d’essais requis pour forcer brutalement une clé. Ainsi, une clé avec une sécurité de 128 bits par exemple correspondrait à effectuer $2^{128}$ essais pour la forcer.  \newline
En cryptographie, une courbe elliptique est un sous groupe qui a une taille donnée $n$. Nous travaillons normalement dans un sous-groupe d’ordre premier $r$, où $r$ divise $n$. Soit $h$ un entier, le cofacteur est égal à $h = n / r$. Il permet d'identifier le nombre de sous-groupe. Ces sous-groupes contiennent tous les points de la courbe.\footnote{\url{https://wizardforcel.gitbooks.io/practical-cryptography-for-developers-book/content/digital-signatures/eddsa-and-ed25519.html}}\footnote{\url{https://crypto.stackexchange.com/questions/56344/what-is-an-elliptic-curve-cofactor}} \newline
Pour chaque point $P$ sur la courbe, le point $hP$ est soit le “point à l’infini”, soit il est de l’ordre de $r$, c’est-à-dire qu’en prenant un point, le multiplier par le cofacteur donne nécessairement un point dans le sous-groupe d’ordre premier $r$. (Le cofacteur est important dans la mesure où il n’est pas égal à 1). \newline
Soit G un point généré sur la courbe elliptique et un sous-groupe de points de la courbe elliptique d’ordre q généré à partir de G.
La paire de clés EdDSA se compose de : \newline

\begin{itemize}
    \item \textbf{clé privée} (entier) : privKey
    \item \textbf{clé publique} (Point de la courbe elliptique) : pubKey = privKey * G \newline
\end{itemize}

La \textbf{clé privée} est générée à partir d’un entier aléatoire appelé “seed”, qui doit avoir une longueur en bits similaire à la clé. La seed est d’abord hachée, puis les derniers bits, correspondant au cofacteur de la courbe \textbf{(8 pour Ed25519 et 4 pour Ed448 afin de garantir que les points seront dans le sous-groupe approprié)} sont effacés et le deuxième bit le plus élevé est mis à 1.

Ces transformations garantissent que la clé privée appartiendra toujours au même sous-groupe de points de la courbe elliptique et que les clés privées auront toujours une longueur de bits similaire (pour se protéger des attaques par canal latéral basées sur la synchronisation).Les attaques par canal latéral utilisent des émissions générées par les appareils numériques (un son, une fluctuation du courant, un déplacement d'air, la génération de chaleur, un écoulement du temps) afin d’observer comment les informations sont traitées. Cela implique que les attaquants peuvent exploiter diverses techniques de canal latéral pour collecter des données et extraire des clés cryptographiques secrètes.
Pour Ed25519, la clé privée est de 32 octets (57 pour Ed448).

La \textbf{clé publique} $PubKey$ est un point sur la courbe elliptique, calculé par la multiplication de points de la courbe elliptique : $pubKey = priv Key * G$  (la clé privée, multipliée par le point générateur $G$ de la courbe). La clé publique est codée en tant que point de la courbe elliptique compressé : la coordonnée $y$, combinée avec le bit le plus bas (la parité) de la coordonnée $x$.
Comme pour la clé privée, la clé publique est de $32$ octets pour Ed25519 et $57$ octets pour Ed448.

\section{Compression et décompression}
Après avoir vu comment générer les clés privées et publiques, il faut maintenant les transformer sous formes d’octet lisible par un programme.


\subsection{Compression (ou encoding)}
Chaque point est codé selon la convention du petit boutiste, c'est-à-dire que chaque point est transformer en une chaine de charactère $h$ de 32 octets avec h[0],...,h[31]. L'entier représenté correspond à $h[0] + 2^8 * h[1] + ... + 2^{248} * h[31]$.
Pour chaque point (x,y) vérifiant $0 <= x,y <p$, on encode d'abord la coordonée $y$ sur une chaine de 32 octets en appliquant la règle vue plus haut. Ensuite, on encode la coordonnée $x$ en copiant le bit le moins significatif sur le bit le plus significatif du dernier octet de $y$. De plus, il faut savoir que le bit le plus significatif du dernier octet vaut toujours 0.

Exemple : 


\subsection{Décompression (ou decoding)}
Décode une chaine de 32 octets n'est pas chose aisée. Pour cela on procède en plusieurs étape :
\begin{enumerate}
    \item On interprète la chaine en tant qu'entier codé sous le format du petit boustiste. Sur cette chaine, on sait que le 255ème bit est le bit le moins significatif de la coordonnée $x$. On a donc la coordonnée $y$ qui vaut les 254 premiers bits. Le 255ème bit est stocké dans x\_0 Maintenant on compare $y$ avec $p$ : s'il est supérieur ou égale, le décodage rate.
    \item Maintenant qu'on a la coordoonée $y$, il faut trouver la coordonnée $x$. Pour cela, la courbe admet une équation $x^2 = (y^2 - 1) / (d * y^2 + 1) \mod p$ qu'il va falloir résoudre. On définit  donc $u = (y^2 - 1)$ et $v = (d * y^2 + 1)$ et on se retrouve avec $x^2 = (u/v)$. Il s'agit maitenant de résoudre cette équation et pour cela on définit $x = (u/v)^{((p+3)/8)}$ obtenue avec $x = (u/v)^{(p+3)/8} = u * v^3 * (u v^7)^{(p-5)/8} \mod p$.
    \item 3 cas s'offre à nous :
    \begin{itemize}
        \item si $v * x^2 = u \mod p$, $x$ est donc la racine carrée.
        \item si $v * x^2 = -u \mod p$, $x = x * 2^{((p-1)/4)}$ et sera la racine carrée.
        \item sinon aucune racine carrée n'est possible et le décodage échoue.
    \end{itemize}
    \item Maintenant, on reprend x\_0 qui va nous permettre de choisir la bonne racine. Si on $x = 0$ et $x\_0 = 1$, le décodage rate. Par contre, si $x\_0$ est différent de $ x \mod 2$, $x = p - x$. 
\end{enumerate}
On a maintenant les 2 coordonnées du points. 

\section{Signer avec EdDSA}

L’algorithme de signature EdDSA prend en entrée un message \textbf{texte (msg) ainsi que la clé privée} EdDSA du signataire (privKey) et produit en sortie une paire d’entiers $\{R,s\}$. 
La signature EdDSA fonctionne de la manière suivante (avec quelques simplifications) :

$EdDSA\_sign(msg, privKey) \rightarrow {R, s }$

\begin{enumerate}
    \item On calcule \textbf{$pubKey = privKey*G$}
    \item On génère un entier secret \textbf{$r = hash$} (hash(privKey) + msg)  \textbf{$\mod q$}
    \item On calcule le point de la clé publique \textbf{$R = r*G$} 
    \item On calcule \textbf{$h = hash$} (R + pubKey + msg) \textbf{$\mod q$}
    \item On calcule \textbf{$s = ( r + h * privKey) \mod q$}
    \item On renvoie la signature \textbf{${R, s}$}
\end{enumerate}


La signature numérique produite vaut 64 octets (32+32 octets) pour Ed25519 et 114 octets (57+57 octets) pour Ed448. Il contient un point compressé \textbf{$R$} + l’entier \textbf{$s$} (confirmant que le signataire connaît le msg et la clé privée).

\section{Vérification de la signature de EdDSA} 

L’algorithme de vérification de signature EdDSA prend en entrée un message texte (msg),\textbf{la clé publique}  EdDSA (pubKey) du signataire ainsi que la signature EdDSA \textbf{$\{R,s\}$} et produit en sortie une valeur booléenne pour dire si la signature est valide ou non. 
La vérification EdDSA fonctionne de la manière suivante (avec quelques simplifications)\footnote{\url{https://cryptobook.nakov.com/digital-signatures/eddsa-and-ed25519}} : 

$EdDSA\_signature\_verify(msg, pubKey, signature {R,s}) \rightarrow valid / invalid$
\begin{enumerate}
    \item On calcule \textbf{$h = hash$} (R + pubKey + msg) \textbf{$\mod q$}
    \item On calcule \textbf{$P1 = s * G$} 
Lors de la signature on a fait le calcul de \textbf{$s = ( r + h * privKey) \mod q$}. Il nous suffit de le remplacer dans l’équation $P1$, ce qui nous donne : 

\textbf{$P1 = (r + h * privKey) \mod q * G$} 
On développe \textbf{$P1 = r * G + h * privKey * G$} 
Or \textbf{$r * G = R$} et \textbf{$privKey * G = pubKey$}  
Donc \textbf{$P1 = R + h * PubKey$}
    \item On calcule \textbf{$P2 = R + h * pubKey$}
    \item En retour, on va tester \textbf{$P1==P2$}
Si ces points $P1$ et $P2$ sont le même point dans la courbe elliptique, cela prouve que le point $P1$, calculé par la clé privée, correspond au point $P2$, créé par sa clé publique correspondante.
\end{enumerate}
\newpage
\section{EdDSA vs ECDSA} 

ECDSA et EdDSA ont une forte similitude que ce soit au niveau de la longueur de leur clé ou bien de leur sécurité liée à la complexité du problème du logarithme discret sur lequel ils reposent. Toutefois, EdDSA, utilisant une famille différente de courbes elliptiques (courbe d’Edwards), offre de légers avantages en vitesse par rapport à l’ECDSA mais cela peut dépendre de l’implémentation. Par ailleurs, contrairement à ECDSA, les signatures EdDSA ne permettent pas de récupérer la clé publique du signataire à partir de la signature et du message. De plus, au lieu de s’appuyer sur un nombre aléatoire pour la valeur nonce tel que le fait ECDSA, EdDSA génère un nonce de manière déterministe sous forme de hachage, ce qui le rend résistant aux collisions. EdDSA sera ainsi d’avantage recommandée pour la plupart des applications modernes.
\footnote{\url{https://goteleport.com/blog/comparing-ssh-keys/}}
\footnote{\url{https://wizardforcel.gitbooks.io/practical-cryptography-for-developers-book/content/digital-signatures/eddsa-and-ed25519.html}}


\section{Sources}
 
paramètre de la courbe edDSA : \url{https://tools.ietf.org/html/rfc8032} \\
scalaire secret : \url{https://blog.filippo.io/using-ed25519-keys-for-encryption/} \\

\url{https://crypto.stackexchange.com/questions/56344/what-is-an-elliptic-curve-cofactor}\\
\url{https://crypto.stackexchange.com/questions/51350/what-is-the-relationship-between-p-prime-n-order-and-h-cofactor-of-an-ell?noredirect=1&lq=1}\\
\url{https://cryptologie.net/article/497/eddsa-ed25519-ed25519-ietf-ed25519ph-ed25519ctx-hasheddsa-pureeddsa-wtf/}\\
\url{https://crypto.stackexchange.com/questions/50405/summarize-the-mathematical-problem-at-the-heart-of-breaking-a-curve25519-public/50414#50414}\\
\url{https://wizardforcel.gitbooks.io/practical-cryptography-for-developers-book/content/asymmetric-key-ciphers/elliptic-curve-cryptography-ecc.html}\\

Implémentation : \url{https://www.oryx-embedded.com/doc/ed25519\_8c\_source.html}
\end{document}
