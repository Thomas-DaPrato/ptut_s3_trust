\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{url}
% les tableaux https://pastebin.com/nPEhiqrW
\usepackage[top=100, bottom=100, left=100, right=100]{geometry}

\title{La courbe P-256 dans ECDSA}
\date{Février 2021}

\begin{document}

\maketitle

\section{Introduction}

Les courbes elliptiques utilisées en cryptographie sont nombreuses. Dans les années 1990, diverses propositions ont été lancées et reprises par les organismes de normalisation, en particulier le NIST (National Institute of Standards and Technology). 
\newline
\newline
Dans FIPS 186 (Normes fédérales de traitement de l'information), une quinzaine de “courbes standards”  ont été décrites; la plus célèbre d'entre elles est la courbe P-256.

\section{La courbe P-256}

La courbe NIST P-256 est une courbe dite de Weierstrass (forme simplifiée de l’équation d’une courbe elliptique) . La génération et la validation de signature à l’aide de cette courbe sont effectuées grâce à l’algorithme de signature numérique à courbe elliptique (ECDSA).  \newline

\begin{figure}[!h]
    \centering
    \includegraphics[scale=0.5]{Graph_stat_DNSSEC.png}
    \caption{Statistique DNSSEC pour .com en 2017 }
\end{figure}

Comme le montre la figure 1, P-256 est la courbe elliptique la plus populaire dans DNSSEC en juin 2017.
Cependant, Ed25519 a été normalisé pour une utilisation dans DNSSEC par RFC8080 en février 2017, suite à des échos relevant d'éventuels problèmes de sécurité avec la courbe P-256.
\newline
\newline
Cela vient ainsi remettre en cause la fiabilité de la courbe P-256 dans la cryptographie.

\section{Les recommandations des courbes dans la cryptographie }

\subsection{Recommandation SafeCurves}

En 2013, Bernstein et Lange ont lancé un site web appelé SafeCurves (\url{https://safecurves.cr.yp.to/index.html})  qui liste les critères pour une analyse structurée de la sécurité des différentes courbes elliptiques. \newline
\newline
Certaines des courbes répertoriées sur ce site sont déployées ou ont été proposées pour déploiement, d’autres ne sont que des exemples destinés à illustrer comment les courbes peuvent ne pas répondre à divers critères de sécurité.\newline
\newline
Le  tableau comparatif ci-dessous (Figure 2) est constitué de 3 colonnes correspondant au nom de la courbe (Curve), à son niveau de sécurité (Safe?) et à ses caractéristiques (Details).

\newpage

\begin{figure}[!h]
    \centering
    \includegraphics[scale=0.5]{SafeCurves_tableau_1.png}
    \caption{Tableau comparatif SafeCurves}
\end{figure}


Si l’on regarde à la 10ème ligne, on peut constater que la courbe P-256 ne satisfait pas tous les critères et est catégorisée comme “unsafe” (pas sécurisée). \newline
\newline
Un autre tableau est disponible sur SafeCurves afin de regarder plus en détail les exigences voulues notamment au niveau des paramètres de base, de sécurité ECDL (problème du logarithme discret des courbes elliptiques) et enfin ECC (cryptographie sur les courbes elliptiques). 

\begin{figure}[!h]
    \centering
    \includegraphics[scale=0.6]{SafeCurves_tableau_2.png}
    \caption{Tableau comparatif détaillé SafeCurves}
\end{figure}

Lorsqu’on regarde de nouveau la courbe P-256 (10ème ligne), on peut noter que quatre critères ne sont pas validés :
\begin{enumerate}
    \item Rigidity (Rigidité) : qui concerne la confiance dans les paramètres de la courbe.
    \item Completeness (Exhaustivité) : qui concerne le traitement des formules d'addition dans leur totalité.
    \item  Indistinguishability from uniform random strings (Indiscernabilité des chaînes aléatoires uniformes) : il s'agit d'un moyen d'encoder un point sur la courbe comme une chaîne aléatoire uniforme.
    \item Ladders (Échelles) : qui fait référence à une manière dont le résultat de la multiplication d’un point \textit{P} par le scalaire \textit{n} (qui se note \textit{nP}) pourraient être calculées.
\end{enumerate}
\newpage

\section{La "porte ouverte" de P-256}

Les paramètres du NIST P-256 sont générés par le hachage de la graine \["C49d3608\quad86e70493\quad6a6678e1\quad139d26b7\quad 819f7e90"\] mais cette graine a été choisie par le NIST sans aucune bonne raison. \newline
Cela a contribué à alimenter des théories quant à d'éventuelles vulnérabilités de la courbe P-256 à une attaque secrète qui s’appliquerait par exemple à une courbe sur un milliard. Par conséquent, si cela se révélait être vrai, cela impliquerait qu’il existe un groupe extrêmement important de courbes faibles pouvant être découvertes.

\subsection{L'attaque de synchronisation du cache}

En décembre 2016, Cesar Pereida Garcia et Billy Bob Brumley dans leur rapport “Constant-time callees with variable-time callers”, exposent la vulnérabilités de P-256 aux attaques de synchronisation du cache dans OpenSSL  1.0.1u. Cette attaque permet de récupérer la clé privée de TLS (Protocole de sécurité de la couche de transport) et SSH (Protocole de communication sécurisé), ce qui pouvait être un inconvénient pour DNSSEC étant donné que les logiciels de serveur DNS qui prennent en charge DNSSEC pouvaient s’appuyer sur des bibliothèques de logiciels tel que OpenSSL.
\newline
\newline
L’attaque de synchronisation du cache se compose de trois phases lors d’un cycle: 
\newline
\begin{enumerate}
    	\item L'attaquant expulse la ligne mémoire cible du cache. 
        \item  L'attaquant attend un certain temps pour que la victime ait la possibilité d'accéder à la ligne mémoire. 
        \item  L'attaquant mesure le temps nécessaire pour recharger la ligne mémoire.\newline
        \newline
        La latence mesurée à la dernière étape indique, si oui ou non la ligne mémoire a été accédée par la victime pendant la deuxième étape de l'attaque, c'est-à-dire identifie les succès de cache et les échecs de cache (savoir si les données demandées pour un traitement se trouvent dans la mémoire cache ou non).
\end{enumerate}


\subsection{L’attaque RASSLE (Return Address Stack based Side-channel Leakage)}

L’Association internationale pour la recherche cryptographique (IACR) publie un article le 23 février 2021 dans lequel les auteurs décrivent un nouveau mécanisme d’attaque exploitant la pile d’adresses de retour (RAS) : une petite pile matérielle présente dans les processeurs modernes permettant de réduire la pénalité de manquement de branche en stockant les adresses de retour de chaque appel de fonctions. \newline

Cette attaque utilise le principe des attaques par canal latéral qui concerne la vulnérabilité du matériel et non celle du programme, c’est-à-dire que le hacker n’a pas besoin de s'introduire dans le système pour récupérer les données : il va se servir de son, d’une fuite électromagnétique, d’un écoulement du temps, etc.., qui vont lui permettre de reconstituer les données.\newline

Divers techniques d’attaques par canal latéral existent mais dans notre cas, ce sont les attaques  par cache (le hacker falsifie et surveille un cache de données qui est partagé entre lui et sa victime) et par synchronisation (le hacker mesure la durée d'exécution pendant le chargement des données vers ou depuis un cache ou une mémoire du processeur) qui vont être utilisées.\newline 

Un processus d’espionnage va créer une condition de débordement dans la pile en la remplissant d’adresses de retour arbitraires et va lutter avec un processus concurrent pour établir un canal latéral de synchronisation entre elles.\newline

Ainsi en exploitant la pile d’adresses de retour on peut réussir à divulguer des bits nonce (chiffre ou nombre qui n’est utilisé qu’une seule fois et qui est émis dans un protocole d'authentification) éphémères d'OpenSSL ECDSA et éventuellement extraire la clé secrète.\newline

Suite à leur découverte, ils nommèrent cette attaque “l'attaque RASSLE”.\newline

Des solutions ont été depuis implémentés dans le NIST 256 pour OpenSSL pour éviter de telles attaques.


\section{Sources}

\url{http://essay.utwente.nl/75354/1/DNSSEC%20curves.pdf}
\newline
\newline
\url{https://en.wikipedia.org/wiki/Timing_attack}\newline
\url{https://www.usenix.org/system/files/conference/usenixsecurity17/sec17-garcia.pdf }\newline
\url{https://www.youtube.com/watch?v=0bT10AFX4NI}\newline
\newline
\url{https://www.researchgate.net/publication/337241514_TPM-FAIL_TPM_meets_Timing_and_Lattice_Attacks}\newline
\url{https://www.iacr.org/cryptodb/data/paper.php?pubkey=30800}\newline
\url{https://www.sciencedirect.com/topics/engineering/network-reduction}\newline
\url{https://www.intertrust.com/blog/side-channel-attacks-strategies-and-defenses/}\newline

\end{document}
