#ifndef EDDSA_H
#define EDDSA_H

#include <memory>

#include "eddsaprivatekey.hpp"
#include "numericutils.hpp"

namespace nsSignature
{
    namespace nsKeys
    {
        template<size_t keySize, class PublicKey>
        class EdDSAPrivateKey;
    } // nsKeys

    template<size_t keySize, class PublicKey>
    class EdDSA
    {
    public:
        virtual ~EdDSA();
        void setPrivateKey(const std::shared_ptr<nsKeys::EdDSAPrivateKey<keySize, PublicKey>>& privateKey);
        void setPublicKey(const std::shared_ptr<PublicKey>& publicKey);

        std::shared_ptr<nsKeys::EdDSAPrivateKey<keySize, PublicKey>> getPrivateKey() const;
        std::shared_ptr<PublicKey> getPublicKey() const;

        virtual std::vector<CryptoPP::byte> sign(const std::string& m, const std::string& context = std::string()) const = 0;
        bool verify(const std::string& m, const std::vector<CryptoPP::byte>& signature, const std::string& context = std::string()) const;

        virtual size_t getSignatureSize() const = 0;

    protected:
        virtual typename PublicKey::PublicKeyPointType decode(const std::vector<CryptoPP::byte> &vec) const = 0;
        std::vector<CryptoPP::byte> prepareMessage(const std::string& m) const;
        static std::vector<CryptoPP::byte> encode(const mpz_class& value);
        static std::vector<CryptoPP::byte> encode(const typename PublicKey::PublicKeyPointType &point);
        virtual std::vector<CryptoPP::byte> hash(const std::vector<CryptoPP::byte>& data, const std::string& context = std::string()) const = 0;

    private:
        std::shared_ptr<nsKeys::EdDSAPrivateKey<keySize, PublicKey>> m_privateKey;
        std::shared_ptr<PublicKey> m_publicKey;
    }; // EdDSA
} // nsSignature

template<size_t keySize, class PublicKey>
std::vector<CryptoPP::byte> nsSignature::EdDSA<keySize, PublicKey>::prepareMessage(const std::string &m) const
{
    if(m.size() % 2 != 0)
        throw std::invalid_argument("message size must be even");

    for(const char& c : m)
        if(!std::isxdigit(c))
            throw std::invalid_argument("non hexadecimal message");

    std::vector<CryptoPP::byte> result(m.size() / 2);

    for(size_t i(0); i < result.size(); ++i)
        result[i] = std::stoul(m.substr(2 * i, 2), nullptr, 16);

    return result;
}

template<size_t keySize, class PublicKey>
nsSignature::EdDSA<keySize, PublicKey>::EdDSA::~EdDSA() {}

template<size_t keySize, class PublicKey>
void nsSignature::EdDSA<keySize, PublicKey>::setPrivateKey(const std::shared_ptr<nsKeys::EdDSAPrivateKey<keySize, PublicKey>>& privateKey)
{
    m_privateKey = privateKey;
}

template<size_t keySize, class PublicKey>
void nsSignature::EdDSA<keySize, PublicKey>::setPublicKey(const std::shared_ptr<PublicKey> &publicKey)
{
    m_publicKey = publicKey;
}

template<size_t keySize, class PublicKey>
std::shared_ptr<nsSignature::nsKeys::EdDSAPrivateKey<keySize, PublicKey>> nsSignature::EdDSA<keySize, PublicKey>::getPrivateKey() const
{
    return m_privateKey;
}

template<size_t keySize, class PublicKey>
std::shared_ptr<PublicKey> nsSignature::EdDSA<keySize, PublicKey>::getPublicKey() const
{
    return m_publicKey;
}

template<size_t keySize, class PublicKey>
bool nsSignature::EdDSA<keySize, PublicKey>::verify(const std::string &m, const std::vector<CryptoPP::byte> &signature, const std::string &context /* = std::string()*/) const
{
    if(nullptr == getPublicKey())
        throw std::runtime_error("private key must be set");

    if(signature.size() != getSignatureSize())
        throw std::invalid_argument("invalid signature size");

    size_t signatureSize(getSignatureSize());

    std::vector<CryptoPP::byte> mHex(prepareMessage(m));

    std::vector<CryptoPP::byte> firstHalf(signatureSize / 2), secondHalf(signatureSize / 2);
    std::copy(signature.begin(), signature.begin() + (signatureSize / 2), firstHalf.begin());
    std::copy(signature.begin() + (signatureSize / 2), signature.end(), secondHalf.begin());

    try
    {
        typename PublicKey::PublicKeyPointType R(decode(firstHalf));
        typename PublicKey::PublicKeyPointType A(getPublicKey()->getPoint());

        mpz_class S(0);
        for(size_t i(secondHalf.size()); i-- > 0; )
            S = 256 * S + secondHalf[i];

        if(S < 0 || S > PublicKey::PublicKeyPointType::getL())
            return false;

        std::vector<CryptoPP::byte> key(encode(getPublicKey()->getPoint()));
        std::vector<CryptoPP::byte> concat(firstHalf.size() + key.size() + mHex.size());
        std::copy(firstHalf.begin(), firstHalf.end(), concat.begin());
        std::copy(key.begin(), key.end(), concat.begin() + firstHalf.size());
        std::copy(mHex.begin(), mHex.end(), concat.begin() + firstHalf.size() + key.size());

        std::vector<CryptoPP::byte> digest(hash(concat, context));

        mpz_class k(0);
        for(size_t i(digest.size()); i-- > 0; )
            k = 256 * k + digest[i];
        k = k % PublicKey::PublicKeyPointType::getL();


        //check [S]B = R + [k]A
        typename PublicKey::PublicKeyPointType sB(PublicKey::PublicKeyPointType::getBasePoint());
        sB.pointMul(S);
        A.pointMul(k);
        typename PublicKey::PublicKeyPointType sumRkA(R);
        sumRkA.add(A);

        return sumRkA == sB;
    }
    catch(const std::invalid_argument& e)
    {
        (void)e;
        return false;
    }
}

template<size_t keySize, class PublicKey>
std::vector<CryptoPP::byte> nsSignature::EdDSA<keySize, PublicKey>::encode(const mpz_class &value)
{
    return numericUtils::bitsetToBytesVector<keySize>(std::bitset<keySize>(value.get_str(2)));
}

template<size_t keySize, class PublicKey>
std::vector<CryptoPP::byte> nsSignature::EdDSA<keySize, PublicKey>::encode(const typename PublicKey::PublicKeyPointType &point)
{
    std::vector<CryptoPP::byte> tempVector(encode(point.getRealY()));
    if((point.getRealX()) % 2 == 1)
        tempVector.back() |= 128;
    return tempVector;
}

#endif // EDDSA_H
