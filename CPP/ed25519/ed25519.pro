TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../../include
INCLUDEPATH += ../../include/ed25519

LIBS += -lgmpxx -lgmp
LIBS += -lcryptopp

SOURCES += \
        ed25519.cpp \
        ed25519ellipticcurvepoint.cpp \
        ed25519privatekey.cpp \
        ed448.cpp \
        ed448ellipticcurvepoint.cpp \
        ed448privatekey.cpp \
        main.cpp

HEADERS += \
    ed25519.h \
    ed25519.h \
    ed25519ellipticcurvepoint.h \
    ed25519privatekey.h \
    ed448.h \
    ed448ellipticcurvepoint.h \
    ed448privatekey.h \
    eddsa.hpp \
    eddsaprivatekey.hpp \
    eddsapublickey.hpp \
    numericutils.hpp \
    publickeytypes.h
