#ifndef PUBLICKEYTYPES_H
#define PUBLICKEYTYPES_H

#include "ed25519ellipticcurvepoint.h"
#include "ed448ellipticcurvepoint.h"
#include "eddsapublickey.hpp"

namespace nsSignature::nsKeys
{
    typedef EdDSAPublicKey<nsEllipticCurve::Ed25519EllipticCurvePoint> Ed25519PublicKey;
    typedef EdDSAPublicKey<nsEllipticCurve::Ed448EllipticCurvePoint> Ed448PublicKey;
}

#endif // PUBLICKEYTYPES_H
