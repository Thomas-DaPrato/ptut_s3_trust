#include "cryptopp/shake.h"

#include "ed448privatekey.h"
#include "ed448.h"

nsSignature::nsKeys::Ed448PrivateKey::Ed448PrivateKey(const std::string &keyString)
    : nsSignature::nsKeys::EdDSAPrivateKey<456, Ed448PublicKey>(keyString)
{}

nsSignature::nsKeys::Ed448PrivateKey::~Ed448PrivateKey() {}

std::vector<CryptoPP::byte> nsSignature::nsKeys::Ed448PrivateKey::hashKey() const
{
    std::vector<CryptoPP::byte> digest(Ed448::SHAKE_OUTPUT_SIZE);

    std::vector<CryptoPP::byte> buffer(getKeyAsVector());

    CryptoPP::SHAKE256 hash(Ed448::SHAKE_OUTPUT_SIZE);
    hash.CalculateDigest(digest.data(), buffer.data(), buffer.size());
    hash.Restart();

    return digest;
}

// throws if the vector is too small
std::vector<CryptoPP::byte>& nsSignature::nsKeys::Ed448PrivateKey::pruneData(std::vector<CryptoPP::byte>& data) const
{
    data.at(0) &= 0xfc;
    data.at(56) = 0;
    data.at(55) |= 0x80;

    return data;
}
