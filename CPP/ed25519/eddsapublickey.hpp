#ifndef EDDSAPUBLICKEY_H
#define EDDSAPUBLICKEY_H

namespace nsSignature::nsKeys
{
    template<class Point>
    class EdDSAPublicKey
    {
    public:
        typedef Point PublicKeyPointType;

        EdDSAPublicKey(const Point& key);
        const Point& getPoint() const;

    private:
        Point m_key;
    }; // EdDSAPublicKey
} // nsSignature

template<class Point>
nsSignature::nsKeys::EdDSAPublicKey<Point>::EdDSAPublicKey(const Point &key)
    : m_key(key)
{}

template<class Point>
const Point& nsSignature::nsKeys::EdDSAPublicKey<Point>::getPoint() const
{
    return m_key;
}


#endif // EDDSAPUBLICKEY_H
