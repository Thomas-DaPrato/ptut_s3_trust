#ifndef ED25519ELLIPTICCURVEPOINT_H
#define ED25519ELLIPTICCURVEPOINT_H

#include "gmp.h"
#include "gmpxx.h"

namespace nsEllipticCurve
{
    class Ed25519EllipticCurvePoint
    {
    public:
        static const Ed25519EllipticCurvePoint& getBasePoint();
        static const mpz_class& getP();
        static const mpz_class& getL();

        static Ed25519EllipticCurvePoint createEd25519Point(const mpz_class& X, const mpz_class& Y, const mpz_class& Z, const mpz_class& T);

        mpz_class getRealX() const;
        mpz_class getRealY() const;

        const mpz_class& getX() const;
        const mpz_class& getY() const;
        const mpz_class& getZ() const;
        const mpz_class& getT() const;

        void add(const Ed25519EllipticCurvePoint& other);
        void pointMul(mpz_class mul);

        bool operator ==(const Ed25519EllipticCurvePoint& other) const;

    private:
        mpz_class m_X;
        mpz_class m_Y;
        mpz_class m_Z;
        mpz_class m_T;

        Ed25519EllipticCurvePoint(const mpz_class& X, const mpz_class& Y, const mpz_class& Z, const mpz_class& T);

    }; // Ed25519EllipticCurvePoint
} // nsEllipticCurve

#endif // ED25519ELLIPTICCURVEPOINT_H
