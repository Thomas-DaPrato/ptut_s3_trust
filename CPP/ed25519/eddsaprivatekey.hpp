#ifndef EDDSAPRIVATEKEY_HPP
#define EDDSAPRIVATEKEY_HPP

#include <bitset>
#include <vector>

#include "cryptopp/sha.h"
#include "cryptopp/hex.h"

#include "gmp.h"
#include "gmpxx.h"

#include "eddsa.hpp"

#include "numericutils.hpp"

namespace nsSignature::nsKeys
{
    template<class PublicKey>
    struct InterpretedPrivateKey
    {
        mpz_class s;
        PublicKey publicKey;
        std::vector<CryptoPP::byte> prefix;
    }; // InterpretedPrivateKey

    template<size_t keySize, class PublicKey>
    class EdDSAPrivateKey
    {
    public:
        EdDSAPrivateKey(const std::string& keyString);
        virtual ~EdDSAPrivateKey();
        std::string getAsString() const;
        std::vector<CryptoPP::byte> getKeyAsVector() const;
        InterpretedPrivateKey<PublicKey> getAssociatedPublicKey() const;

    protected:
        virtual std::vector<CryptoPP::byte> hashKey() const = 0;
        virtual std::vector<CryptoPP::byte>& pruneData(std::vector<CryptoPP::byte>& data) const = 0;

    private:
        std::bitset<keySize> m_key;
    }; // EdDSAPrivateKey
} // nsSignature

template<size_t keySize, class PublicKey>
nsSignature::nsKeys::EdDSAPrivateKey<keySize, PublicKey>::EdDSAPrivateKey::~EdDSAPrivateKey() {}

template<size_t keySize, class PublicKey>
nsSignature::nsKeys::EdDSAPrivateKey<keySize, PublicKey>::EdDSAPrivateKey(const std::string& keyString)
{
    if(keySize != keyString.size()*4)
        throw std::invalid_argument("invalid key size");

    for(size_t i(0); i < keyString.size(); ++i)
        if(std::isxdigit(keyString[i]) == 0)
            throw std::invalid_argument("non hexadecimal key");

    mpz_class temp(keyString, 16);
    std::string binStr(temp.get_str(2));
    if(binStr.size() != keySize)
        binStr = std::string(keySize - binStr.size(), '0') + binStr;
    for(size_t i(0); i < binStr.size(); ++i)
        m_key[m_key.size() - 1 - i] = (binStr[i] == '1' ? true : false);
} // EdDSAPrivateKey

template<size_t keySize, class PublicKey>
std::string nsSignature::nsKeys::EdDSAPrivateKey<keySize, PublicKey>::getAsString() const
{
    return m_key.to_string();
} // getAsString

template<size_t keySize, class PublicKey>
std::vector<CryptoPP::byte> nsSignature::nsKeys::EdDSAPrivateKey<keySize, PublicKey>::getKeyAsVector() const
{
    return numericUtils::bitsetToBytesVectorInvert(m_key);
} // getKeyAsVector

template<size_t keySize, class PublicKey>
nsSignature::nsKeys::InterpretedPrivateKey<PublicKey> nsSignature::nsKeys::EdDSAPrivateKey<keySize, PublicKey>::getAssociatedPublicKey() const
{
    std::vector<CryptoPP::byte> digest(hashKey());

    std::vector<CryptoPP::byte> buffer2(digest.size() / 2);
    std::copy(digest.begin(), digest.begin() + (digest.size() / 2), buffer2.begin());
    std::vector<CryptoPP::byte> prefix(digest.size() / 2);
    std::copy(digest.begin() + (digest.size() / 2), digest.end(), prefix.begin());

    pruneData(buffer2);

    mpz_class s(0);
    mpz_class power(1);
    for(size_t i(0); i < buffer2.size(); ++i)
    {
        s += power * buffer2[i];
        power *= 256;
    }

    typename PublicKey::PublicKeyPointType B = PublicKey::PublicKeyPointType::getBasePoint();
    B.pointMul(s);

    return {s, B, prefix};
} // getAssociatedPublicKey

#endif // EDDSAPRIVATEKEY_HPP
