#ifndef ED448ELLIPTICCURVEPOINT_H
#define ED448ELLIPTICCURVEPOINT_H

#include "gmp.h"
#include "gmpxx.h"

namespace nsEllipticCurve
{
    class Ed448EllipticCurvePoint
    {
    public:
        static const Ed448EllipticCurvePoint& getBasePoint();
        static const mpz_class& getP();
        static const mpz_class& getL();

        static Ed448EllipticCurvePoint createEd448Point(const mpz_class& X, const mpz_class& Y, const mpz_class& Z);

        mpz_class getRealX() const;
        mpz_class getRealY() const;

        const mpz_class& getX() const;
        const mpz_class& getY() const;
        const mpz_class& getZ() const;

        void add(const Ed448EllipticCurvePoint& other);
        void pointMul(mpz_class mul);

        bool operator ==(const Ed448EllipticCurvePoint& other) const;

    private:
        mpz_class m_X;
        mpz_class m_Y;
        mpz_class m_Z;

        Ed448EllipticCurvePoint(const mpz_class& X, const mpz_class& Y, const mpz_class& Z);

    }; // Ed448EllipticCurvePoint
} // nsEllipticCurve

#endif // ED448ELLIPTICCURVEPOINT_H
