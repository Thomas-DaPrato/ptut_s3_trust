#include "ed448.h"

#include "cryptopp/shake.h"

// static members

const std::string nsSignature::Ed448::DOM4_ED448_CONSTANT_STRING = "SigEd448";

const mpz_class nsSignature::Ed448::_p("726838724295606890549323807888004534353641360687318060281490199180612328166730772686396383698676545930088884461843637361053498018365439");
const mpz_class nsSignature::Ed448::_d("726838724295606890549323807888004534353641360687318060281490199180612328166730772686396383698676545930088884461843637361053498018326358");
const nsSignature::Ed448::Point nsSignature::Ed448::NEUTRAL_ELEMENT(Point::createEd448Point(0, 1, 1));
const mpz_class nsSignature::Ed448::_L("181709681073901722637330951972001133588410340171829515070372549795146003961539585716195755291692375963310293709091662304773755859649779");
const size_t nsSignature::Ed448::SIGNATURE_SIZE = 114;
const nsSignature::Ed448::Point nsSignature::Ed448::_B(Point::createEd448Point(
        mpz_class("224580040295924300187604334099896036246789641632564134246125461686950415467406032909029192869357953282578032075146446173674602635247710"),
        mpz_class("298819210078481492676017930443930673437544040154080242095928241372331506189835876003536878655418784733982303233503462500531545062832660"),
        1));
const size_t nsSignature::Ed448::SHAKE_OUTPUT_SIZE = 114;


nsSignature::Ed448::~Ed448() {}

std::vector<CryptoPP::byte> nsSignature::Ed448::sign(const std::string& m, const std::string& context /* = std::string() */) const
{
    if(nullptr == getPrivateKey())
        throw std::runtime_error("private key must be set");

    if(context.size() > 255)
        throw std::invalid_argument("Context too long (max : 255 bytes)");

    std::vector<CryptoPP::byte> mHex(prepareMessage(m));
    std::vector<CryptoPP::byte> contextHex(prepareMessage(context));

    nsKeys::InterpretedPrivateKey<nsKeys::Ed448PublicKey> pk(getPrivateKey()->getAssociatedPublicKey());

    std::vector<CryptoPP::byte> dom4Result(dom4(contextHex));
    std::vector<CryptoPP::byte> concat(dom4Result);

    concat.insert(concat.end(), pk.prefix.begin(), pk.prefix.end());
    concat.insert(concat.end(), mHex.begin(), mHex.end());

    CryptoPP::SHAKE256 hash(SHAKE_OUTPUT_SIZE);
    std::vector<CryptoPP::byte> digest(SHAKE_OUTPUT_SIZE);
    hash.CalculateDigest(digest.data(), concat.data(), concat.size());
    hash.Restart();   

    mpz_class r(0);
    for(size_t i(digest.size()); i-- > 0; )
        r = r * 256 + digest[i];
    r = r % _L;

    Point B2(Point::getBasePoint());
    B2.pointMul(r);

    std::vector<CryptoPP::byte> encodedB2(encode(B2));
    std::vector<CryptoPP::byte> message(dom4Result);
    message.insert(message.end(), encodedB2.begin(), encodedB2.end()); // concatenate B2 (R in the RFC 8032)
    std::vector<CryptoPP::byte> encoded2(encode(pk.publicKey.getPoint()));
    message.insert(message.end(), encoded2.begin(), encoded2.end()); // concatenate A
    message.insert(message.end(), mHex.begin(), mHex.end()); // concatenate message (PH(M) with PH the identity function)

    hash.CalculateDigest(digest.data(), message.data(), message.size());
    hash.Restart();

    mpz_class numericDigest(0);
    for(size_t i(digest.size()); i-- > 0; )
        numericDigest = 256 * numericDigest + digest[i];
    numericDigest %= _L;
    mpz_class S(r + numericDigest * pk.s);
    S = S % _L;


    std::vector<CryptoPP::byte> result2(encode(S));
    encodedB2.insert(encodedB2.end(), result2.begin(), result2.end());
    return encodedB2;
}

nsSignature::Ed448::Point nsSignature::Ed448::decode(const std::vector<CryptoPP::byte> &vec) const
{
    if(vec.size() != SIGNATURE_SIZE / 2)
        throw std::invalid_argument("invalid size");

    // First, interpret the string as an integer in little-endian
    mpz_class y(0);
    for(size_t i(vec.size()); i-- > 0; )
        y = y * 256 + vec[i];

    // Bit 455 of this number is the least significant bit of the x-coordinate
    mpz_class x_0(y >> 455);

    // The y-coordinate is recovered simply by clearing this bit
    mpz_class mask(1);
    mask <<= 455;
    mask -= 1;
    y &= mask;

    if(y >= _p)
        throw std::invalid_argument("cannot be decoded");

    mpz_class u(y * y - 1);
    mpz_class v(_d * y * y - 1);
    mpz_class x2(u);
    mpz_class temp;
    mpz_invert(temp.get_mpz_t(), v.get_mpz_t(), _p.get_mpz_t());
    x2 *= temp;

    temp = (_p + 1) / 4;

    mpz_class x;
    mpz_powm(x.get_mpz_t(), x2.get_mpz_t(), temp.get_mpz_t(), _p.get_mpz_t());

    if(((v * x * x) - u) % _p != 0)
        throw std::invalid_argument("cannot be decoded");


    if(x == 0 && x_0 == 1)
        throw std::invalid_argument("cannot be decoded");

    if(x_0 != x % 2)
        x = _p - x;

    return Point::createEd448Point(x, y, 1);
}

std::vector<CryptoPP::byte> nsSignature::Ed448::dom4(const std::vector<CryptoPP::byte> &context) const
{
    std::vector<CryptoPP::byte> concat;

    concat.insert(concat.end(), DOM4_ED448_CONSTANT_STRING.begin(), DOM4_ED448_CONSTANT_STRING.end());
    concat.push_back(0); // phFlag = 0 for Ed448
    concat.push_back(static_cast<CryptoPP::byte>(context.size()));
    concat.insert(concat.end(), context.begin(), context.end());

    return concat;
}

size_t nsSignature::Ed448::getSignatureSize() const
{
    return SIGNATURE_SIZE;
}

std::vector<CryptoPP::byte> nsSignature::Ed448::hash(const std::vector<CryptoPP::byte> &data, const std::string& context /* = std::string() */) const
{
    CryptoPP::SHAKE256 hash(SHAKE_OUTPUT_SIZE);
    std::vector<CryptoPP::byte> digest(SHAKE_OUTPUT_SIZE);

    std::vector<CryptoPP::byte> preparedData(dom4(prepareMessage(context)));
    preparedData.insert(preparedData.end(), data.begin(), data.end());

    hash.CalculateDigest(digest.data(), preparedData.data(), preparedData.size());
    return digest;
}
