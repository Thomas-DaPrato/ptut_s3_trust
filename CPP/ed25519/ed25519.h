#ifndef ED25519_H
#define ED25519_H

#include <memory>
#include <exception>
#include <vector>

#include "gmpxx.h"

#include "ed25519ellipticcurvepoint.h"
#include "eddsa.hpp"
#include "publickeytypes.h"
#include "numericutils.hpp"

namespace nsSignature
{
    class Ed25519 : public EdDSA<256, nsKeys::Ed25519PublicKey>
    {
    public:
        typedef nsEllipticCurve::Ed25519EllipticCurvePoint Point;

        static constexpr size_t KEY_SIZE = 256;

        static const mpz_class _d;
        static const mpz_class _p;
        static const mpz_class _L;
        static const Point NEUTRAL_ELEMENT;
        static const Point _B;
        static const size_t SIGNATURE_SIZE;

        virtual ~Ed25519() override;

        virtual std::vector<CryptoPP::byte> sign(const std::string& m, const std::string& context = std::string()) const override;

        virtual size_t getSignatureSize() const override;

    private:
        virtual Point decode(const std::vector<CryptoPP::byte> &vec) const override;
        virtual std::vector<CryptoPP::byte> hash(const std::vector<CryptoPP::byte>& data, const std::string& context = std::string()) const override;
    }; // ed25519
} // nsSignature

#endif // ED25519_H
