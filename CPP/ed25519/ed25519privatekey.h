#ifndef ED25519PRIVATEKEY_H
#define ED25519PRIVATEKEY_H

#include "eddsaprivatekey.hpp"
#include "publickeytypes.h"

namespace nsSignature::nsKeys
{
    class Ed25519PrivateKey : public EdDSAPrivateKey<256, Ed25519PublicKey>
    {
    public:
        Ed25519PrivateKey(const std::string& keyString);
        virtual ~Ed25519PrivateKey() override;

    private:
        virtual std::vector<CryptoPP::byte> hashKey() const override;
        virtual std::vector<CryptoPP::byte> &pruneData(std::vector<CryptoPP::byte> &data) const override;
    }; // Ed25519PrivateKey
} // nsSignature

#endif // ED25519PRIVATEKEY_H
