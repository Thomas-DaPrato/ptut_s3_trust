TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../myLibraries/include

LIBS += -lgmp -lgmpxx
LIBS += -L ../myLibraries/libs -lRSASHA256
LIBS += -L ../myLibraries/libs -lSHA256
LIBS += -L ../myLibraries/libs -lRSA


SOURCES += \
        main.cpp
