#ifndef RSASHA256SIGNATURE_H
#define RSASHA256SIGNATURE_H

#include <array>
#include <memory>
#include "gmpxx.h"
#include "SHA256/sha256.h"
#include "RSA/rsa.h"
#include "RSA/rsaprivatekey.h"
#include "RSA/rsapublickey.h"

namespace nsSignature
{
    class RsaSha256Signature
    {
        std::shared_ptr<nsCrypto::RSAPrivateKey> m_privateKey;
        std::shared_ptr<nsCrypto::RSAPublicKey> m_publicKey;
        nsHash::SHA256 m_SHA256; // contient les données
        nsCrypto::RSA m_RSA;
        std::vector<uint8_t> m_signature;

        static const std::array<uint8_t, 19> DERencodingTofTheDigestInfoValue;
        static const unsigned tLen;

    public:
        RsaSha256Signature(const std::string & input = std::string());
        std::vector<uint8_t> sign(bool file);
        bool check(bool file, const std::vector<uint8_t>& signature);

        void setPrivateKey(const std::shared_ptr<nsCrypto::RSAPrivateKey>& prK);
        void setPublicKey(const std::shared_ptr<nsCrypto::RSAPublicKey> &puK);
        std::shared_ptr<nsCrypto::RSAPrivateKey> getPrivateKey() const;
        std::shared_ptr<nsCrypto::RSAPublicKey> getPublicKey() const;
        void setInput(const std::string& input);

    private:
        std::vector<uint8_t> encode(bool file, unsigned long emLen);
        mpz_class convertOS2IP(const std::vector<uint8_t>& v) const;
        std::vector<uint8_t> convertI2OSP(mpz_class x, unsigned long length) const;
    };
}

#endif // RSASHA256SIGNATURE_H
