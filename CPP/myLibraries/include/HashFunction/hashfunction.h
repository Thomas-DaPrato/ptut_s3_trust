#ifndef HASHFUNCTION_H
#define HASHFUNCTION_H

#include <array>
#include <iomanip>
#include <sstream>

namespace nsHash
{
    class HashFunction
    {
    protected:
        std::string m_inputData;
        bool m_valid;

    public:
        HashFunction(const std::string & input = std::string());

        bool isValid() const;
        void setInputData(const std::string& inputData);
        const std::string& getInputData() const;

        virtual std::string getResultAsString() const = 0;

        virtual void computeString() = 0;
        virtual void computeFile()   = 0;
	virtual ~HashFunction();
    };
}

#endif // HASHFUNCTION_H
