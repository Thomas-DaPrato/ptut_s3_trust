#ifndef SHA256_H
#define SHA256_H

#include <array>
#include <vector>
#include <istream>

#include "HashFunction/hashfunction.h"

namespace nsHash
{
    class SHA256 : public HashFunction
    {
        static const std::array<uint32_t, 64> K;
        static const std::array<uint32_t, 8> registersInit;
        static constexpr unsigned blocksSize = 512;

        std::array<uint32_t, 8> m_hash;

    public:
        SHA256(const std::string & input = std::string());
        virtual void computeString();
        virtual void computeFile();
        const std::array<uint32_t, 8>& getResult() const;
        std::array<uint8_t, 32> getResultBytes() const;

	virtual std::string getResultAsString() const;

    private:
        void compute(std::istream& stream);
        std::array<uint32_t, 16> toTab(const std::array<uint8_t, blocksSize / 8> &tab) const;
        void addPadding(std::array<uint8_t, 16*4> &tab, size_t pos, uint64_t l, bool add1) const;
        uint32_t Ch(uint32_t x, uint32_t y, uint32_t z) const;
        uint32_t Maj(uint32_t x, uint32_t y, uint32_t z) const;
        uint32_t RotateRight(uint32_t val, unsigned short n) const;
        uint32_t Sigma0(uint32_t x) const;
        uint32_t Sigma1(uint32_t x) const;
        uint32_t PetitSigma0(uint32_t x) const;
        uint32_t PetitSigma1(uint32_t x) const;
        void resetHash();

    };
}

#endif // SHA256_H
