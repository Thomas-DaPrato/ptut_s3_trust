#ifndef NUMERICUTILS_H
#define NUMERICUTILS_H

#include <gmpxx.h>

namespace nsNumericUtils
{
    mpz_class pgcd(mpz_class a, mpz_class b);
    mpz_class modularExp(const mpz_class& a, const mpz_class& b, const mpz_class& mod);
    mpz_class randomCoprime(const mpz_class& val);
    mpz_class modularInverse(const mpz_class& val, const mpz_class& mod);
    mpz_class randomPrime_n_bits(unsigned long bits);
    bool isPrime(const mpz_class& val);
} // namespace nsNumericUtils

#endif // NUMERICUTILS_H
