TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../../include/SHA256
INCLUDEPATH += ../../include/HashFunction
INCLUDEPATH += ../../include/

SOURCES += \
        main.cpp \
        sha256.cpp \
        ../HashFunction/hashfunction.cpp

HEADERS += \
    sha256.h
