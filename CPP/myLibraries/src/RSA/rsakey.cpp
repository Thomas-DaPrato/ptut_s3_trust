#include "rsakey.h"

nsCrypto::RSAKey::RSAKey(const mpz_class& exp, const mpz_class& n)
    : m_exponent(exp), m_n(n)
{}

const mpz_class& nsCrypto::RSAKey::getN() const { return m_n; }
const mpz_class& nsCrypto::RSAKey::getExponent() const { return m_exponent; }

void nsCrypto::RSAKey::setExponent(const mpz_class& exp) { m_exponent = exp; }
void nsCrypto::RSAKey::setN(const mpz_class& n) { m_n = n; }
