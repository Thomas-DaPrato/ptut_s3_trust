TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../../include
INCLUDEPATH += ../../include/SignatureRSA_SHA256
INCLUDEPATH += ../../include/HashFunction

LIBS += -lgmpxx -lgmp
LIBS += -L ../../libs -lRSA
LIBS += -L ../../libs -lSHA256

SOURCES += \
        main.cpp \
        rsasha256signature.cpp

HEADERS += \
    rsasha256signature.h
