TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../../include/SHA1
INCLUDEPATH += ../../include/HashFunction
INCLUDEPATH += ../../include/

SOURCES += \
        main.cpp \
        sha1.cpp \
        ../HashFunction/hashfunction.cpp

HEADERS += \
    sha1.h
