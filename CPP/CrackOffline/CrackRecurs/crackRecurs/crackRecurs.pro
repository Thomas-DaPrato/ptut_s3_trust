TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lcryptopp

SOURCES += \
        cracker.cpp \
        main.cpp

HEADERS += \
    cracker.h
