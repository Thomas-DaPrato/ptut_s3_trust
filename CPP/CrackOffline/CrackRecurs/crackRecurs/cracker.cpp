#include "cryptopp/sha.h"

#include "cracker.h"

using namespace std;

const std::string Password::Cracker::LOWER_CASE = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

const std::string Password::Cracker::ALPHA = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                              'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

const std::string Password::Cracker::ALPHA_NUM = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                                  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

const std::string Password::Cracker::ALPHA_NUM_SPECIAL = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                                          'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                          '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '!', '"', '#', '$', '%', '&', '\'','(', ')', '*', '+', ',', '-', '.', '/',
                                                          ':', ';', '<', '=', '>', '?', '@', '[', '\\',']', '^', '_', '`', '{', '|', '}', '~'};

Password::Cracker::Cracker(const string& hashToCrack)
    : m_hashToCrack(hashToCrack), m_result(""), m_nbTests(0)
{}

void Password::Cracker::setHashToCrack(const std::string& hashToCrack)
{
    m_hashToCrack = hashToCrack;
    m_nbTests = 0;
}

string Password::Cracker::getResult()
{
    return m_result;
}

unsigned long long Password::Cracker::getNbTests()
{
    return m_nbTests;
}

bool Password::Cracker::crack(const size_t maxSize, const string &charSet)
{
    m_nbTests = 0;
    if(charSet.size() == 0 || m_hashToCrack.size() != m_sha256.DigestSize())
    {
        m_result = "";
        return false;
    }

    std::string hash;
    const std::string emptyString("");
    m_sha256.Update((const byte*)emptyString.data(), emptyString.size());
    hash.resize(m_sha256.DigestSize());
    m_sha256.Final((byte*)&hash[0]);
    m_sha256.Restart();
    ++m_nbTests;

    if(hash == m_hashToCrack)
    {
        m_result = emptyString;
        return true;
    }

    // V1 ---
    string gen(maxSize, '\0');
    string substr;
    return crackRecursV1(charSet, gen, 0, hash, substr);
    // V1 ---

    // V2 ---
    //for(size_t s(1); s <= maxSize; ++s)
    //{
    //    string gen(s, '\0');
    //    if(crackRecursV2(charSet, gen, 0, hash))
    //        return true;
    //}
    //return false;
    // V2 ---

}

bool Password::Cracker::crackRecursV1(const string &charSet, string& gen, size_t pos, string& hash, string substr)
{
    if(pos >= gen.size())
    {
        m_result = "";
        return false;
    }

    for(const char& c : charSet)
    {
        gen[pos] = c;
        substr = gen.substr(0, pos + 1);

        m_sha256.Update((const byte*)substr.data(), substr.size());
        m_sha256.Final((byte*)&hash[0]);
        m_sha256.Restart();

        ++m_nbTests;

        if(hash == m_hashToCrack)
        {
            m_result = substr;
            return true;
        }

        if(crackRecursV1(charSet, gen, pos + 1, hash, substr))
            return true;
    }
    return false;
}

bool Password::Cracker::crackRecursV2(const string &charSet, string& gen, size_t pos, string& hash)
{
    if(pos >= gen.size())
    {
        m_sha256.Update((const byte*)gen.data(), gen.size());
        m_sha256.Final((byte*)&hash[0]);
        m_sha256.Restart();
        ++m_nbTests;

        if(hash == m_hashToCrack)
        {
            m_result = gen;
            return true;
        }

        m_result = "";
        return false;
    }

    for(const char& c : charSet)
    {
        gen[pos] = c;
        if(crackRecursV2(charSet, gen, pos + 1, hash))
            return true;
    }
    return false;
}
