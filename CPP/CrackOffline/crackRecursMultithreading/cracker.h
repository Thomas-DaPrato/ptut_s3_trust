#ifndef CRACKER_H
#define CRACKER_H

#include <string>
#include <array>
#include "cryptopp/sha.h"
#include "threadsmanager.h"

namespace Password
{
    class Cracker
    {
    public:
        static const std::string LOWER_CASE;
        static const std::string ALPHA;
        static const std::string ALPHA_NUM;
        static const std::string ALPHA_NUM_SPECIAL;

    private:
        std::string m_hashToCrack;
        std::string m_result;

        std::atomic<unsigned long long> m_nbTests;

    public:
        Cracker(const std::string &hashToCrack);
        void setHashToCrack(const std::string& hashToCrack);
        std::string getResult();
        unsigned long long getNbTests();
        bool crack(const size_t maxSize, const std::string &charSet);

    private:
        void crackRecursTrigger(const std::string &charSet, size_t size, ThreadsManager& allThreads);
        bool crackRecurs(const std::string &charSet, std::string& gen,
                                                 size_t pos, std::string& hash, ThreadsManager &allThreads, CryptoPP::SHA256& sha256);

    }; // class Cracker
} // namespace Password

#endif // CRACKER_H
