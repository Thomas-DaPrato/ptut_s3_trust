#include <iostream>
#include <cryptopp/filters.h>
#include <cryptopp/hex.h>
#include <cryptopp/sha.h>
#include <cryptopp/md5.h>
#include <vector>
#include <algorithm>
#include <memory>

const char begchar = 32;
const char endchar = 127;


std::shared_ptr<std::vector<std::string>> initVector(){
    char i (begchar);
    std::shared_ptr<std::vector<std::string>> VFhash(new std::vector<std::string>());
    VFhash->resize(endchar-begchar);

    for(std::string& str1 : *VFhash){
        std::string str2 (1,i);
        str1 = str2;
        i+=1;
    }
    return VFhash;
}

std::pair<std::string, unsigned long long> crackSHA256(const std::string & hash, CryptoPP::SHA256& hashSHA256){
    unsigned long long nb(endchar - begchar);
    std::string Fhash;
    std::shared_ptr<std::vector<std::string>> VFhash(initVector());
    std::shared_ptr<std::vector<std::string>> VFhash2(new std::vector<std::string>());
    size_t vecSize(endchar - begchar);

    hashSHA256.Restart();

    for(const std::string & str : *VFhash){
        Fhash.clear();
        hashSHA256.Update((const byte*)str.data(), str.size());
        Fhash.resize(hashSHA256.DigestSize());
        hashSHA256.Final((byte*)&Fhash[0]);
        hashSHA256.Restart();
        if(hash == Fhash)
            return {VFhash->back(), nb};
    }
    while(true){
        std::string tempString;
        for(const std::string & str : *VFhash){
            for(char c(begchar); c < endchar ; ++c){
                tempString = str + c;
                hashSHA256.Update((const byte*)tempString.data(), tempString.size());
                Fhash.resize(hashSHA256.DigestSize());
                hashSHA256.Final((byte*)&Fhash[0]);
                hashSHA256.Restart();

                VFhash2->push_back(tempString);
                ++nb;
                if(hash == Fhash){
                    return {VFhash2->back(), nb};
                }
            }
        }
        std::swap(VFhash, VFhash2);
        VFhash2->clear();
        vecSize = vecSize*(endchar - begchar);
        VFhash2->reserve(vecSize);
    }
}


std::pair<std::string, unsigned long long> crackMD5(const std::string & hash, CryptoPP::MD5& hashMD5){
    unsigned long long nb(endchar - begchar);
    std::string Fhash;
    std::shared_ptr<std::vector<std::string>> VFhash(initVector());
    std::shared_ptr<std::vector<std::string>> VFhash2(new std::vector<std::string>());
    size_t vecSize(endchar - begchar);

    hashMD5.Restart();

    for(const std::string & str : *VFhash){
        Fhash.clear();
        hashMD5.Update((const byte*)str.data(), str.size());
        Fhash.resize(hashMD5.DigestSize());
        hashMD5.Final((byte*)&Fhash[0]);
        hashMD5.Restart();
        if(hash == Fhash)
            return {VFhash->back(), nb};
    }
    while(true){
        std::string tempString;
        for(const std::string & str : *VFhash){
            for(char c(begchar); c < endchar ; ++c){
                tempString = str + c;
                hashMD5.Update((const byte*)tempString.data(), tempString.size());
                Fhash.resize(hashMD5.DigestSize());
                hashMD5.Final((byte*)&Fhash[0]);
                hashMD5.Restart();

                VFhash2->push_back(tempString);
                ++nb;
                if(hash == Fhash){
                    return {VFhash2->back(), nb};
                }
            }
        }
        std::swap(VFhash, VFhash2);
        VFhash2->clear();
        vecSize = vecSize*(endchar - begchar);
        VFhash2->reserve(vecSize);
    }
}

int main(){

    std::string message = "hash"; // doit être non vide
    bool sha256(false); // modifier ceci pour changer de fonction de hachage
    // true  -> SHA256
    // false -> MD5


    if(sha256) {
    CryptoPP::SHA256 hashSHA256;
    std::string hash;
    hashSHA256.Update((const byte*)message.data(), message.size());
    hash.resize(hashSHA256.DigestSize());
    hashSHA256.Final((byte*)&hash[0]);
    hashSHA256.Restart();

    std::cout << "debut" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::pair<std::string, unsigned long long> resultat(crackSHA256(hash, hashSHA256));
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

    std::cout << "Message : \""<< resultat.first <<"\""<< std::endl;
    std::cout << "Nb tests : " << resultat.second << std::endl;
    std::cout << "temps d'execution : " << duration.count() << " millisecondes" << std::endl;
    }
    else {
        CryptoPP::MD5 hashMD5;
        std::string hash;
        hashMD5.Update((const byte*)message.data(), message.size());
        hash.resize(hashMD5.DigestSize());
        hashMD5.Final((byte*)&hash[0]);
        hashMD5.Restart();

        std::cout << "debut" << std::endl;
        auto start = std::chrono::high_resolution_clock::now();
        std::pair<std::string, unsigned long long> resultat(crackMD5(hash, hashMD5));
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

        std::cout << "Message : \""<< resultat.first <<"\""<< std::endl;
        std::cout << "Nb tests : " << resultat.second << std::endl;
        std::cout << "temps d'execution : " << duration.count() << " millisecondes" << std::endl;
    }
    return 0;
}

