\documentclass[class=article, crop=false]{standalone}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{hyperref}

\begin{document}

\section{SHA-1}

\subsection{Introduction}

Cette partie est une description du fonctionnement de l'algorithme de hachage SHA-1 publiée en 1995 par le NIST (Federal Information Processing Standard). Cette version est sortie peu après celle du SHA-0 et est en faite une version complexifiée de celle-ci, mais sera mise de côté suite à la  collision entre deux PDF (par Google : \url{https://shattered.it/}), et alors nous verrons l'apparition du SHA-256 beaucoup plus résistant aux collisions que SHA-1. Le résultat de cet algorithme est un haché de 160 bits, quelle que soit la taille des données fournies en entrée. \\

Le traitement effectué est très similaire à SHA-256.

\subsection{Fonctionnement}
Avant le traitement principal, SHA-1 prépare les données à traiter; c'est l'étape de bourrage.

\subsubsection{Bourrage}
Notons \(l\) la taille en bits du message avant bourrage.\\
Les données à traiter doivent être découpées en blocs de 512 bits. Pour effectuer cette étape, un bit de valeur 1 est ajouté à la fin du message (quelle que soit sa taille) puis on ajoute \(k\) bits à 0, où \(k\) est le plus petit entier naturel vérifiant \(l + 1 + k \equiv 448\) mod \(512\). Enfin, un nouveau bloc de 64 bits contenant la valeur \(l\) est ajouté.

Pour terminer, chaque bloc est représenté sous la forme de 16 mots de 32 bits (blocs de 512 bits). \\

\textit{Notons ici que la taille des données traitées par SHA-1 ne peut pas excéder \(2^{64} - 1\) du fait de la taille du champ réservé lors de l'étape de bourrage. Cela représente plus de 2 millions de terra-octets. Cette remarque concerne également SHA-256, une autre fonction de hahchage décrite dans ce dossier.}

\subsubsection{Traitement principal}

Cette phase utilise principalement ces 3 fonctions qui définissent \(f_t\) :

\[
f_t = 
\begin{cases} 
      Ch(x, y, z) = (x \land y) \lor (\neg x \land z) &  0 \leq t \leq 19\\
      Par(x, y, z) = x \oplus y \oplus z &  20 \leq t \leq 39\\
      Maj(x, y, z) = (x \land y) \lor (x \land z) \lor (y \land z) &  40 \leq t \leq 59\\
      Par(x, y, z) = x \oplus y \oplus z &  60 \leq t \leq 79\\
\end{cases}
\]

L’algorithme utilise aussi 80 constantes \( K_{0},K_{1},...,K_{79}\), elles sont rangées de la manière suivante : \\

\[
K[t] = 
\begin{cases} 
    0x5a827999 &  0 \leq t \leq 19\\
    0x6ed9ebal &  20 \leq t \leq 39\\
    0x8f1bbcdc &  40 \leq t \leq 59\\
    0xca62c1d6 &  60 \leq t \leq 79\\
\end{cases}
\]\\

Ces valeurs ont été calculées de la manière suivante :\\
Pour la première \(2^{30}*\sqrt{2}\) ce qui nous donne pour la partie entière : \(1518500249\) donc en hexadécimal \(0x5a827999\).\\
Les valeurs suivantes sont calculées de la même manière en prenant à la place de \(\sqrt{2}\) respectivement,\(\sqrt{3}\), \(\sqrt{5}\) et \(\sqrt{10}\).\\  

On définit ici les notations utilisées tout au long de cette documentation : \newline
\\
\(a, b, c, d, e\) : 5 registres de travail (mots de 32 bits). \newline
\(hash[i]\) : \(i^{eme}\) mot de 32 bits de hachage intermédiaire. \newline
\(M[i]\) : \(i^{eme}\) bloc du résultat du bourrage. \newline
\(N\) :  le nombre de blocs. \newline
\(K\) : tableau contenant les constantes définies ci-dessus. \newline
\(W\) : tableau de 80 mots de 32 bits.

\paragraph{Initialisation :}
Les variables de hachage sont initialisées avec les valeurs suivantes : 
\[hash[0] = 0x67452301\]
\[hash[1] = 0xefcdab89\]
\[hash[2] = 0x98badcfe\]
\[hash[3] = 0x10325476\]
\[hash[4] = 0xc3d2e1f0\]
\\

Ces 5 valeurs ont été choisies car elles ne comportent pas de propriété particulière. Ces nombres sont des "Nothing up my sleeve numbers", ils sont choisis dans l'optique de montrer qu'ils ne contiennent pas de porte dérobée (aucune propriété particulière utilisable par ceux la connaissant). Ils utilisent essentiellement différentes manières de compter de \(1\) à \(f\) en hexadécimal, c'est par cette construction simpliste que l'absence de porte dérobée est garantie. Les valeurs des 80 constantes sont également choisies dans le même but.

\paragraph{Hachage : }
Pour chaque bloc \(B = M[i]\) avec \(0 \leq i \leq N - 1\) de 512 bits (découpé en éléments de 32 bits), on note \(B[i]\) le \(i^{eme}\) mot de 32 bits de \(M[i]\).
\newline

Initialisation du tableau \(W\)
\[
W[t] = 
\begin{cases} 
    B[t] & 0 \leq t \leq 15\\
    rol(W[t - 3] \oplus W[t - 8] \oplus W[t - 14] \oplus W[t - 16],1) & 16 \leq t \leq 79
\end{cases}
\]
\\
Où \(rol(x, n)\) (resp. \(ror(x, n)\)) représente une rotation des bits de x vers la gauche (resp. droite) de n bits et \(x << n\) (resp. \(x >> n\)) un décalage de n bits vers la gauche (resp. droite) (\(x\) est un mot de 32 bits). \\\\
Suite à ce traitement nous avons étendu un bloc \(B\) de 16 mots du message en un bloc temporaire \(W\) de 80 mots.

On initialise ensuite les variables de travail avec la valeur de hachage, soit : 

\[a = hash[0]\]
\[b = hash[1]\]
\[c = hash[2]\]
\[d = hash[3]\]
\[e = hash[4]\] 

Il faut à présent compresser le bloc temporaire \(W\) de 80 mots, pour cela, on utilise les registres de travail.
Pour \(0 \leq j \leq 79\) (boucle)

\[a = rol(a,5)+f_j(b,c,d) + e + K[j] + W[j]\]
\[b = a\]
\[c = rol(b,30)\]
\[d = c\]
\[e = d\] 

Une fois les 80 tours de la boucle effectués, on peut calculer les nouvelles valeurs de hachage intermédiaires :

\[hash[0] = a + hash[0]\]
\[hash[1] = b + hash[1]\]
\[hash[2] = c + hash[2]\]
\[hash[3] = d + hash[3]\]
\[hash[4] = e + hash[4]\]

\paragraph{Résultat : }
Une fois ces opérations effectuées pour tous les blocs du résultat du bourrage, le résultat du hachage est contenu \(hash\). \newline
Notons \(||\) l'opérateur de concaténation, le résultat est :
\[hash[0] ~||~ hash[1] ~||~ hash[2] ~||~ hash[3] ~||~ hash[4]\]

\paragraph{Référence \\}
\url{https://tools.ietf.org/html/rfc3174}

\end{document}

